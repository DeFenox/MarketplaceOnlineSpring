package com.defenox.marketplace.controller;

import com.defenox.marketplace.model.Item;
import com.defenox.marketplace.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by tokarev on 08.02.2017.
 */
@Controller
public class ItemController {

    private ItemService itemService;

    @Autowired(required = true)
    @Qualifier(value = "itemService")
    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    @RequestMapping(value = "items", method = RequestMethod.GET)
    public String listItems(Model model){
        model.addAttribute("item", new Item());
        model.addAttribute("listItems", this.itemService.listItems());

        return "items";
    }

    @RequestMapping(value = "/items/add", method = RequestMethod.POST)
    public String addItem(@ModelAttribute("item") Item item){
        if(item.getId() == 0){
            this.itemService.addItem(item);
        }else {
            this.itemService.updateItem(item);
        }
        return "redirect:/items";
    }

    @RequestMapping("/items/remove/{id}")
    public String removeItem(@PathVariable("id") int id){
        this.itemService.removeItem(id);

        return "redirect:/items";
    }

    @RequestMapping("/items/edit/{id}")
    public String editItem(@PathVariable("id") int id, Model model){
        model.addAttribute("item", this.itemService.getItemById(id));
        model.addAttribute("listItems", this.itemService.listItems());

        return "items";
    }

    @RequestMapping("/items/view/{id}")
    public String viewItem(@PathVariable("id") int id, Model model){
        model.addAttribute("item", this.itemService.getItemById(id));

        return "/items/view";
    }
}
