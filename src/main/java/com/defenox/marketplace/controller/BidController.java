package com.defenox.marketplace.controller;

import com.defenox.marketplace.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

/**
 * Created by tokarev on 08.02.2017.
 */
@Controller
public class BidController {

    private BidService bidService;

    @Autowired(required = true)
    @Qualifier(value = "bidService")
    public void setBidService(BidService bidService) {
        this.bidService = bidService;
    }


}
