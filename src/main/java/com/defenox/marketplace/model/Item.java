package com.defenox.marketplace.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by tokarev on 07.02.2017.
 */
@Entity
@Table(name = "ITEMS")
public class Item {

    @Id
    @Column(name = "BID_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "USER_ID")
    private User seller;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "START_PRICE")
    private int startPrice;

    @Column(name = "TIME_LEFT")
    private Date timeLeft;

    @Column(name = "START_BIDDING_DATE")
    private Date startBiddingDate;

    @Column(name = "BUY_IT_NOW")
    private boolean buyItNow;

    @Column(name = "BID_INCREMENT")
    private int bidIncrement;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(int startPrice) {
        this.startPrice = startPrice;
    }

    public Date getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(Date timeLeft) {
        this.timeLeft = timeLeft;
    }

    public Date getStartBiddingDate() {
        return startBiddingDate;
    }

    public void setStartBiddingDate(Date startBiddingDate) {
        this.startBiddingDate = startBiddingDate;
    }

    public boolean isBuyItNow() {
        return buyItNow;
    }

    public void setBuyItNow(boolean buyItNow) {
        this.buyItNow = buyItNow;
    }

    public int getBidIncrement() {
        return bidIncrement;
    }

    public void setBidIncrement(int bidIncrement) {
        this.bidIncrement = bidIncrement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (id != item.id) return false;
        if (startPrice != item.startPrice) return false;
        if (buyItNow != item.buyItNow) return false;
        if (bidIncrement != item.bidIncrement) return false;
        if (seller != null ? !seller.equals(item.seller) : item.seller != null) return false;
        if (title != null ? !title.equals(item.title) : item.title != null) return false;
        if (description != null ? !description.equals(item.description) : item.description != null) return false;
        if (timeLeft != null ? !timeLeft.equals(item.timeLeft) : item.timeLeft != null) return false;
        return startBiddingDate != null ? startBiddingDate.equals(item.startBiddingDate) : item.startBiddingDate == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (seller != null ? seller.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + startPrice;
        result = 31 * result + (timeLeft != null ? timeLeft.hashCode() : 0);
        result = 31 * result + (startBiddingDate != null ? startBiddingDate.hashCode() : 0);
        result = 31 * result + (buyItNow ? 1 : 0);
        result = 31 * result + bidIncrement;
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", seller=" + seller +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", startPrice=" + startPrice +
                ", timeLeft=" + timeLeft +
                ", startBiddingDate=" + startBiddingDate +
                ", buyItNow=" + buyItNow +
                ", bidIncrement=" + bidIncrement +
                '}';
    }
}
