package com.defenox.marketplace.model;

/**
 * Created by tokarev on 07.02.2017.
 */

import javax.persistence.*;

@Entity
@Table(name = "BIDS")
public class Bid {

    @Id
    @Column(name = "BID_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "USER_ID")
    private User bidder;

    @Column(name = "BID")
    private int bid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public User getBidder() {
        return bidder;
    }

    public void setBidder(User bidder) {
        this.bidder = bidder;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bid bid1 = (Bid) o;

        if (id != bid1.id) return false;
        if (bid != bid1.bid) return false;
        if (item != null ? !item.equals(bid1.item) : bid1.item != null) return false;
        return bidder != null ? bidder.equals(bid1.bidder) : bid1.bidder == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (item != null ? item.hashCode() : 0);
        result = 31 * result + (bidder != null ? bidder.hashCode() : 0);
        result = 31 * result + bid;
        return result;
    }

    @Override
    public String toString() {
        return "Bid{" +
                "id=" + id +
                ", item=" + item +
                ", bidder=" + bidder +
                ", bid=" + bid +
                '}';
    }
}
