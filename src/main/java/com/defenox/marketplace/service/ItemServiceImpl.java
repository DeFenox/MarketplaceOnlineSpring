package com.defenox.marketplace.service;

import com.defenox.marketplace.dao.ItemDao;
import com.defenox.marketplace.model.Item;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
@Service
public class ItemServiceImpl implements ItemService {

    private ItemDao itemDao;

    public void setItemDao(ItemDao itemDao){
        this.itemDao = itemDao;
    }

    @Transactional
    public void addItem(Item item) {
        this.itemDao.addItem(item);
    }

    @Transactional
    public void updateItem(Item item) {
        this.itemDao.updateItem(item);
    }

    @Transactional
    public void removeItem(int id) {
        this.itemDao.removeItem(id);
    }

    @Transactional
    public Item getItemById(int id) {
        return this.itemDao.getItemById(id);
    }

    @Transactional
    public List<Item> listItems() {
        return this.itemDao.listItems();
    }
}
