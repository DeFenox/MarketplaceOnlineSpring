package com.defenox.marketplace.service;

import com.defenox.marketplace.dao.BidDao;
import com.defenox.marketplace.model.Bid;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
@Service
public class BidServiceImpl implements BidService {

    private BidDao bidDao;

    public void setBidDao(BidDao bidDao){
        this.bidDao = bidDao;
    }

    @Transactional
    public void addBid(Bid bid) {
        this.bidDao.addBid(bid);
    }

    @Transactional
    public void updateBid(Bid bid) {
        this.bidDao.updateBid(bid);
    }

    @Transactional
    public void removeBid(int id) {
        this.bidDao.removeBid(id);
    }

    @Transactional
    public Bid getBidById(int id) {
        return this.bidDao.getBidById(id);
    }

    @Transactional
    public List<Bid> listBids() {
        return this.bidDao.listBids();
    }
}
