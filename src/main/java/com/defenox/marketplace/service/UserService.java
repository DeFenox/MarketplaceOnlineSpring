package com.defenox.marketplace.service;

import com.defenox.marketplace.model.User;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
public interface UserService {
    public void addUser(User user);
    public void updateUser(User user);
    public void removeUser(int id);
    public User getUserById(int id);
    public List<User> listUsers();
}
