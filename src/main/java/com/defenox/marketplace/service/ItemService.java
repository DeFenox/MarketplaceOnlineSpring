package com.defenox.marketplace.service;

import com.defenox.marketplace.model.Item;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
public interface ItemService {
    public void addItem(Item item);
    public void updateItem(Item item);
    public void removeItem(int id);
    public Item getItemById(int id);
    public List<Item> listItems();
}
