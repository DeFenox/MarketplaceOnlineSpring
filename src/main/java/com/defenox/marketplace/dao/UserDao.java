package com.defenox.marketplace.dao;

import com.defenox.marketplace.model.User;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
public interface UserDao {
    public void addUser(User bid);

    public void updateUser(User bid);

    public void removeUser(int id);

    public User getUserById(int id);

    public List<User> listUsers();
}
