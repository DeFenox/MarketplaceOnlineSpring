package com.defenox.marketplace.dao;

import com.defenox.marketplace.model.Bid;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
public interface BidDao {
    public void addBid(Bid bid);

    public void updateBid(Bid bid);

    public void removeBid(int id);

    public Bid getBidById(int id);

    public List<Bid> listBids();
}
