package com.defenox.marketplace.dao;

import com.defenox.marketplace.model.Bid;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
@Repository
public class BidDaoImpl implements BidDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public void addBid(Bid bid) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(bid);
    }

    public void updateBid(Bid bid) {
        Session session = sessionFactory.getCurrentSession();
        session.update(bid);
    }

    public void removeBid(int id) {
        Session session = sessionFactory.getCurrentSession();
        Bid bid = (Bid) session.load(Bid.class, new Integer(id));
        if(bid != null){
            session.delete(bid);
        }
    }

    public Bid getBidById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Bid) session.load(Bid.class, new Integer(id));
    }

    public List<Bid> listBids() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from BIDS").list();
    }
}
