package com.defenox.marketplace.dao;

import com.defenox.marketplace.model.Item;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
public interface ItemDao {
    public void addItem(Item bid);

    public void updateItem(Item bid);

    public void removeItem(int id);

    public Item getItemById(int id);

    public List<Item> listItems();
}
