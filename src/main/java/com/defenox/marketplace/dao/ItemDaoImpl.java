package com.defenox.marketplace.dao;

import com.defenox.marketplace.model.Item;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tokarev on 08.02.2017.
 */
@Repository
public class ItemDaoImpl implements ItemDao {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public void addItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(item);
    }

    public void updateItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    public void removeItem(int id) {
        Session session = sessionFactory.getCurrentSession();
        Item item = (Item) session.load(Item.class, new Integer(id));
        if(item != null){
            session.delete(item);
        }
    }

    public Item getItemById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Item) session.load(Item.class, new Integer(id));
    }

    public List<Item> listItems() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from itemS").list();
    }
}
